﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Douchette
{
    public class Verif
    {
        public static bool Lengthcc(string value)
        {
            bool result = false;
            if (value.Length == 13)
            {
                result = true;
            }
            return result;
        }

        public static bool Nbsep(string value)
        {
            bool result = false;
            int count = 0;
            for (int iter = 0; iter < 13; iter++)
            {
                if(value[iter]=='-')
                {
                    count++;
                }
            }
            if (count == 2)
            {
                result = true;
            }
            return result;
        }

        public static bool Possep(string value)
        {
            bool result = false;
            if (value[3] == '-' && value[8] == '-')
            {
                result = true;
            }
            return result;
        }

        public static void Cut(string value, ref string type, ref int date, ref int refprod)
        {

            for (int iter = 0; iter < 4; iter++)
            {
                if(iter < 3)
                {
                    type += value[iter];
                    if (type[iter] < 'A' || type[iter] > 'Z')
                    {
                        throw new System.ArgumentException("out of range", "type");
                    }
                }
                date += value[iter + 4];
                refprod += value[iter + 9];
            }
            if(date>2019)
            {
                throw new System.ArgumentException("your product is futuristic", "date");
            }
        }
    }
}
