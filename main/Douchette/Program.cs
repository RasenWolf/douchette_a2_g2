﻿using System;

namespace Douchette
{
    class Program
    {
        static void Main(string[] args)
        {
            bool veriflength, verifnbsep, verifpossep;
            int date=0, refprod = 0;
            string type="";
            Console.WriteLine("saisir valeur de code barre"); //saisie temporaire pour tester nos fonctions en temps réel
            string scanned = Console.ReadLine();
            veriflength = Verif.Lengthcc(scanned);
            verifnbsep = Verif.Nbsep(scanned);
            verifpossep = Verif.Possep(scanned);
            Verif.Cut(scanned,
                      ref type,
                      ref date,
                      ref refprod);
            database.SetConnection();
            database.Add(type, date, refprod);
            database.Dataclose();
        }
    }
}
